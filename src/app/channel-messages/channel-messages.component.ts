import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import { Message } from '../message';
import { ChatService } from '../services/chat.service';

@Component({
  selector: 'app-channel-messages',
  templateUrl: './channel-messages.component.html',
  styleUrls: ['./channel-messages.component.css']
})
export class ChannelMessagesComponent implements OnInit {
  channel;
  messages: Message[] = [];

  newMessage : string;


  constructor(private activatedRoute : ActivatedRoute, private chatService: ChatService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.channel = parseInt(params.get('id'));
    });
    this.getMessagesToDisplay();
  }

  getMessagesToDisplay() {
    this.chatService.displayMessages(this.channel).subscribe(
      (messages: Message[]) => this.messages = messages
    );
  }

  sendMessage() {
    const newMessage: Message = {
      user: 'Toto',
      channel: this.channel,
      content: this.newMessage
    }

    this.chatService.addMessage(newMessage).subscribe(
      (message: Message) => this.messages.push(message)
    );

    this.newMessage = '';
  }
}
