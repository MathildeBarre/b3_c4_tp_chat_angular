export class Message {
    id?: number
    channel: number
    user: string
    content: string
}
