import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Channel } from '../channel';
import { Message } from '../message'
@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient) { }

  displayChannels(): Observable<Channel[]> {
    return this.http.get<Channel[]>(`${environment.backend}channels`);
  }

  displayMessages(channelId): Observable<Message[]>{
    return this.http.get<Message[]>(`${environment.backend}channels/${channelId}/messages`)
  }

  addMessage(message: Message): Observable<Message> {
    return this.http.post<Message>(`${environment.backend}messages`, message)
  }
}
