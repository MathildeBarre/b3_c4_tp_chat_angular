import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ChannelsComponent } from './channels/channels.component';
import { ChannelMessagesComponent } from './channel-messages/channel-messages.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { 
    path: 'channels/:id', component: ChannelsComponent,
    children: [
      { path: 'channels/:id/messages', component: ChannelMessagesComponent},
      { path: 'channels/:id', redirectTo: 'channels/:id/messages', pathMatch: 'full'},
    ]
  },
  
  
  //{ path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
