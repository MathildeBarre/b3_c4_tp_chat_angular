import { Component, OnInit } from '@angular/core';
import { ChatService } from '../services/chat.service';
import { Channel } from '../channel';

@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.css']
})
export class ChannelsComponent implements OnInit {
  channels: Channel[] = [];

  constructor(private chatService: ChatService) { }

  ngOnInit() {
    this.displaySideBarChannels();
  }

  private displaySideBarChannels() {
    this.chatService.displayChannels().subscribe(
      (channels: Channel[]) => this.channels = channels
    );
  }
}
